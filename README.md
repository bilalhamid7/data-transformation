To run application:

npm install (or yarn install)

npm run dev - This runs the app at http://localhost:3000/

To run tests:
npm run test

Libraries used

React 16,
Typescript,
RxJS,
Lodash

Testing frameworks:

Jest,
Enzyme