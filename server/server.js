const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const port = process.env.PORT || 5000;

const fs = require('fs');
const path = require('path');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

app.get('/api/getDictionaries', (req, res) => {
  try {
    fs.readFile(path.join(__dirname, '/data/dictionary.json'), function (err, data) {
      const dictionaryJson = JSON.parse(data);
      const results = dictionaryJson.data;
      res.send({data: results});
    });
  } catch (e) {
    console.log(e);
    res.send(JSON.stringify({data: []}));
  }
});

app.listen(port, () => console.log(`Listening on port ${port}`));
