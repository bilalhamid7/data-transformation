import Builder from 'builder-factory';

import { IMappedDictionary } from '../models';

export const dictionaryFactory = Builder.create<IMappedDictionary>({
  id: '1',
  dictionary: {
    'Midnight Black': 'Black'
  },
  name: 'Phone'
});
