import { Action } from 'redux';
import { Actions } from './index';
import { IDictionaryForm, IMappedDictionary } from '../models';

export type ActionTypes =
  IApplicationLoad |
  IFetchDictionaries |
  IFetchDictionariesSuccess |
  IEditDictionary |
  IDeleteDictionary |
  IEditDictionaryItem |
  IDeleteDictionaryItem |
  IADD_DICTIONARY_ITEM_CLICK |
  IADD_DICTIONARY_ITEM |
  IADD_DICTIONARY |
  INEW_DICTIONARY_CLICK |
  IUpdateFormField;

export interface IApplicationLoad extends Action {
  type:Actions.APPLICATION_LOAD;
}

export interface IFetchDictionaries extends Action {
  type:Actions.FETCH_DICTIONARIES;
}

export interface IFetchDictionariesSuccess {
  type:Actions.FETCH_DICTIONARIES_SUCCESS;
  payload:{ dictionaries:IMappedDictionary[] };
}

export interface IUpdateFormField {
  type:Actions.UPDATE_FORM_FIELD;
  payload:{ [id:string]:string; };
}

export interface IEditDictionary {
  type:Actions.EDIT_DICTIONARY;
  payload:{ id:string }
}

export interface IDeleteDictionary {
  type:Actions.DELETE_DICTIONARY;
  payload:{ id:string };
}

export interface IEditDictionaryItem {
  type:Actions.EDIT_DICTIONARY_ITEM;
  payload:{ key:string };
}

export interface IDeleteDictionaryItem {
  type:Actions.DELETE_DICTIONARY_ITEM;
  payload:{ key:string };
}

export interface IADD_DICTIONARY {
  type:Actions.ADD_DICTIONARY;
  payload:{ dictionaryForm:IDictionaryForm };
}

export interface IADD_DICTIONARY_ITEM_CLICK {
  type:Actions.ADD_DICTIONARY_ITEM_CLICK;
}

export interface IADD_DICTIONARY_ITEM {
  type:Actions.ADD_DICTIONARY_ITEM;
}

export interface INEW_DICTIONARY_CLICK {
  type:Actions.NEW_DICTIONARY_CLICK;
}
