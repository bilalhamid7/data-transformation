import { ActionTypes } from './actionTypes';
import { IDictionaryForm, IMappedDictionary } from '../models';

export enum Actions {
  APPLICATION_LOAD = 'APPLICATION_LOAD',

  FETCH_DICTIONARIES = 'FETCH_DICTIONARIES',
  FETCH_DICTIONARIES_SUCCESS = 'FETCH_DICTIONARIES_SUCCESS',

  ADD_DICTIONARY = 'ADD_DICTIONARY',
  EDIT_DICTIONARY = 'EDIT_DICTIONARY',
  DELETE_DICTIONARY = 'DELETE_DICTIONARY',

  NEW_DICTIONARY_CLICK = 'NEW_DICTIONARY_CLICK',

  ADD_DICTIONARY_ITEM_CLICK = 'ADD_DICTIONARY_ITEM_CLICK',
  ADD_DICTIONARY_ITEM = 'ADD_DICTIONARY_ITEM',
  EDIT_DICTIONARY_ITEM = 'EDIT_DICTIONARY_ITEM',
  DELETE_DICTIONARY_ITEM = 'DELETE_DICTIONARY_ITEM',

  UPDATE_FORM_FIELD = 'UPDATE_FORM_FIELD'
}

export const applicationLoad = ():ActionTypes => ({
  type: Actions.APPLICATION_LOAD
});

export const fetchDictionaries = ():ActionTypes => ({
  type: Actions.FETCH_DICTIONARIES
});

export const fetchDictionariesSuccess = (dictionaries:IMappedDictionary[]):ActionTypes => ({
  type: Actions.FETCH_DICTIONARIES_SUCCESS,
  payload: {
    dictionaries
  }
});

export const updateFormField = (payload:{ [id:string]:any }):ActionTypes => ({
  type: Actions.UPDATE_FORM_FIELD,
  payload
});

export const editDictionary = (id:string):ActionTypes => ({
  type: Actions.EDIT_DICTIONARY,
  payload: {
    id
  }
});

export const newDictionaryClick = ():ActionTypes => ({
  type: Actions.NEW_DICTIONARY_CLICK
});

export const deleteDictionary = (id:string):ActionTypes => ({
  type: Actions.DELETE_DICTIONARY,
  payload: {
    id
  }
});

export const editDictionaryItem = (key:string):ActionTypes => ({
  type: Actions.EDIT_DICTIONARY_ITEM,
  payload: {
    key
  }
});

export const deleteDictionaryItem = (key:string):ActionTypes => ({
  type: Actions.DELETE_DICTIONARY_ITEM,
  payload: {
    key
  }
});

export const onAddDictionaryItemClick = ():ActionTypes => ({
  type: Actions.ADD_DICTIONARY_ITEM_CLICK
});


export const addDictionaryItemClick = ():ActionTypes => ({
  type: Actions.ADD_DICTIONARY_ITEM
});

export const addDictionary = (dictionaryForm:IDictionaryForm):ActionTypes => ({
  type: Actions.ADD_DICTIONARY,
  payload: {
    dictionaryForm
  }
});
