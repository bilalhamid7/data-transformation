import {
  updateFormField,
  editDictionaryItem,
  deleteDictionaryItem,
  onAddDictionaryItemClick,
  addDictionary
} from '../../actions';

export default {
  updateFormField,
  addDictionary,
  onAddDictionaryItemClick,
  editDictionaryItem,
  deleteDictionaryItem
};
