import { createSelector } from 'reselect';
import { notEmpty, validate } from '../../utils/validation';
import { getDictionaryForm } from '../../selectors/dictionarySelectors';

const rules = {
  from: [
    {test: notEmpty, message: 'cannot be empty'}
  ],
  to: [
    {test: notEmpty, message: 'cannot be empty'}
  ]
};

export const getDictionaryFormErrors = createSelector(
  getDictionaryForm,
  (dictionaryForm) => {
    const errors = validate(rules)(dictionaryForm);
    if (dictionaryForm.dictionary.hasOwnProperty(dictionaryForm.from)) {
      return {
        ...errors,
        'duplicates': 'cannot contain duplicates'
      }
    }
    return errors;
  }
);
