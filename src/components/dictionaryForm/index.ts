import { connect } from 'react-redux';

import { IDispatchProps, IProps, DictionaryFormView } from './views/dictionaryFormView';
import selector from './selector';
import actions from './actions';

export const DictionaryForm = connect<IProps, IDispatchProps>(
  selector,
  actions
)(DictionaryFormView);
