import { createSelector } from 'reselect';
import { getDictionaryForm } from '../../selectors/dictionarySelectors';
import { getDictionaryFormErrors } from './formRules';

export default createSelector(
  getDictionaryForm,
  getDictionaryFormErrors,
  (dictionaryForm, formErrors) => ({
    dictionaryForm,
    formErrors: dictionaryForm.isDirty ? formErrors : {}
  })
);
