import * as React from 'react';
import { isEmpty, size } from 'lodash';

import { Link } from 'react-router-dom';
import { IDictionaryForm, IMappedDictionary } from '../../../models';
import { TextInput } from '../../common/forms/TextInput';
import { constants } from '../../../constants';

export interface IProps {
  dictionaryForm:IDictionaryForm;
  formErrors:{ [field:string]:string };
}

export interface IDispatchProps {
  updateFormField:(payload:{ [id:string]:any }) => any;
  addDictionary:(dictionary:IMappedDictionary) => any;
  onAddDictionaryItemClick:() => any;
  editDictionaryItem:(key:string) => any;
  deleteDictionaryItem:(key:string) => any;
}

export type DictionaryFormProps = IProps & IDispatchProps;

export const DictionaryFormView = (props:DictionaryFormProps) => (
  <div className="container">
    <div className="columns">
      <div className="column is-6 is-offset-2">
        <div className="box">
          <form>
            <div>
              <TextInput
                componentId={'from'}
                label={'From'}
                value={props.dictionaryForm.from}
                onFieldChange={props.updateFormField}
                error={props.formErrors.from}
              />
            </div>
            <div>
              <TextInput
                componentId={'to'}
                label={'To'}
                value={props.dictionaryForm.to}
                onFieldChange={props.updateFormField}
                error={props.formErrors.to || props.formErrors.duplicates}
              />
            </div>

            <button
              className="button is-primary"
              type="button"
              onClick={props.onAddDictionaryItemClick}
              disabled={!isEmpty(props.formErrors)}
            >
              Add item
            </button>
          </form>
        </div>
      </div>
      <div className="column">
        <button
          className="button is-primary is-pulled-left"
          onClick={() => props.addDictionary(props.dictionaryForm)}
          disabled={size(props.dictionaryForm.dictionary) === 0}
        >
          Save Dictionary
        </button>
        <Link to={constants.routes.root}>
          <button className="button is-primary is-pulled-right">
            Back
          </button>
        </Link>
      </div>
    </div>

    <div className="columns">
      <div className="column is-offset-2">
        <h2>Current Items</h2>
        <table className="table is-hoverable">
          <thead>
          <tr>
            <th>From</th>
            <th>To</th>
            <th></th>
            <th></th>
          </tr>
          </thead>
          <tbody>
          {Object.keys(props.dictionaryForm.dictionary).map((key:string, index:number) => (
            <tr key={index}>
              <td>{key}</td>
              <td>{props.dictionaryForm.dictionary[key]}</td>
              <td>
                <button
                  className="button is-primary is-small"
                  onClick={() => props.editDictionaryItem(key)}
                >
                  Edit
                </button>
              </td>
              <td>
                <button
                  className="button is-danger is-small"
                  onClick={() => props.deleteDictionaryItem(key)}
                >
                  Delete
                </button>
              </td>
            </tr>
          ))}
          </tbody>
        </table>
      </div>
    </div>
  </div>
);
