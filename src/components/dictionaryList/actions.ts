import { editDictionary, deleteDictionary } from '../../actions';

export default {
  editDictionary,
  deleteDictionary
};
