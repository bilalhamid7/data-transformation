import { connect } from 'react-redux';

import { IProps, IDispatchProps, DictionaryListView } from './views/dictionaryListView';
import selector from './selector';
import actions from './actions';

export const DictionaryList = connect<IProps, IDispatchProps>(
  selector,
  actions
)(DictionaryListView);
