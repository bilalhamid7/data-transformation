import { createSelector } from 'reselect';
import { getDictionaries } from '../../selectors/dictionarySelectors';

export default createSelector(
  getDictionaries,
  (dictionaries) => ({
    dictionaries
  })
);
