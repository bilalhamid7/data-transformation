import * as React from 'react';
import { size } from 'lodash';

import { IMappedDictionary } from '../../../models';

export interface IProps {
  dictionaries:IMappedDictionary[];
}

export interface IDispatchProps {
  editDictionary:(id:string) => any;
  deleteDictionary:(id:string) => any;
}

export const DictionaryListView = (props:IProps & IDispatchProps) => (
  <>
    <h2>Dictionary List</h2>
    <table className="table is-hoverable">
      <thead>
      <tr>
        <th>ID</th>
        <th>Items</th>
        <th></th>
        <th></th>
      </tr>
      </thead>
      <tbody>
      {props.dictionaries.map((item:IMappedDictionary, index:number) => (
        <tr key={index}>
          <td>{item.id}</td>
          <td>{size(item.dictionary)}</td>
          <td>
            <button
              className="button is-primary is-small"
              onClick={() => props.editDictionary(item.id)}
            >
              Edit
            </button>
          </td>
          <td>
            <button
              className="button is-danger is-small"
              onClick={() => props.deleteDictionary(item.id)}
            >
              Delete
            </button>
          </td>
        </tr>
      ))}
      </tbody>
    </table>
  </>
);
