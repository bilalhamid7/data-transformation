import * as React from 'react';
import { DictionaryList } from '../dictionaryList';
import 'bulma/css/bulma.css';

export interface IProps {
  newDictionaryClick:() => any;
}

export const HomePageView = (props:IProps) => (
  <div className="App">
    <div className="columns">
      <div className="column is-offset-2 is-8">
        <DictionaryList/>
      </div>
      <div className="column">
        <button className="button is-primary is-pulled-right" onClick={() => props.newDictionaryClick()}>
          New Dictionary
        </button>
      </div>
    </div>
  </div>
);
