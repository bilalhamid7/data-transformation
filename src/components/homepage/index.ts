import { connect } from 'react-redux';

import { IProps, HomePageView } from './homePage';
import actions from './actions';

export const HomePage = connect<IProps>(
  undefined,
  actions
)(HomePageView);
