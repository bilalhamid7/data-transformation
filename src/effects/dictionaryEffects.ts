import { Action, AnyAction } from 'redux';
import { combineEpics, Epic, ofType } from 'redux-observable';
import { concat, of, iif, EMPTY } from 'rxjs';
import { ignoreElements, map, switchMap, tap, filter } from 'rxjs/internal/operators';
import { ajax } from 'rxjs/internal/observable/dom/ajax';
import { isNil, isEmpty } from 'lodash';
import toastr from 'toastr';

import {
  Actions, addDictionaryItemClick,
  fetchDictionariesSuccess, updateFormField
} from '../actions';
import { IMappedDictionary, IAppState } from '../models';
import { constants } from '../constants';
import { mapDictionaries } from '../mappers/dictionaryMapper';
import { getDictionaries } from "../selectors/dictionarySelectors";
import { push } from 'react-router-redux';
import { getDictionaryFormErrors } from '../components/dictionaryForm/formRules';
import { defaultDictionaryForm } from "../reducers/initialState";

export const loadDictionaryEffect:Epic<AnyAction, AnyAction, IAppState> = (action$) =>
  action$.pipe(
    ofType(Actions.FETCH_DICTIONARIES),

    switchMap(() =>
      ajax.getJSON(`${constants.BASE_API_URL}/getDictionaries`)
        .pipe(
          map((response:{ data:IMappedDictionary[] }) => fetchDictionariesSuccess(
            mapDictionaries(response.data))
          )
        )
    )
  );

export const editDictionaryEffect:Epic<AnyAction, AnyAction, IAppState> = (action$, state$) =>
  action$.pipe(
    ofType(Actions.EDIT_DICTIONARY),
    map((action) => action.payload.id),
    map((id:string) => {
      const dictionaries = getDictionaries(state$.value);
      return dictionaries.find((item:IMappedDictionary) => item.id === id);
    }),
    filter((dictionary) => !isNil(dictionary)),
    switchMap((dictionary) =>
      concat(
        of(push(constants.routes.dictionary)),
        of(updateFormField(
          {
            ...dictionary
          }
        ))
      )
    )
  );

export const newDictionaryClickEffect:Epic<AnyAction, AnyAction, IAppState> = (action$) =>
  action$.pipe(
    ofType(Actions.NEW_DICTIONARY_CLICK),
    switchMap(() =>
      concat(
        of(push(constants.routes.dictionary)),
        of(updateFormField(
          {
            ...defaultDictionaryForm
          }
        ))
      )
    )
  );

export const deleteDictionaryEffect:Epic<AnyAction, AnyAction, IAppState> = (action$, state$) =>
  action$.pipe(
    ofType(Actions.DELETE_DICTIONARY),
    map((action) => action.payload.id),
    map((id:string) => {
      const dictionaries = getDictionaries(state$.value);
      return dictionaries.filter((item:IMappedDictionary) => item.id !== id);
    }),
    switchMap((dictionaries) => concat(
      of(fetchDictionariesSuccess(dictionaries)),
      of(tap(toastr.success('Deleted dictionary'))).pipe(
        ignoreElements()
      ),
    ))
  );

export const onAddDictionaryItemClickEffect:Epic<AnyAction, AnyAction, IAppState> = (action$, state$) =>
  action$.pipe(
    ofType(Actions.ADD_DICTIONARY_ITEM_CLICK),
    switchMap(() => concat(
      iif(
        () => !state$.value.dictionaryForm.isDirty,
        of(updateFormField({isDirty: true})),
        EMPTY
      ),
      iif(
        () => isEmpty(getDictionaryFormErrors(state$.value)),
        of(addDictionaryItemClick()),
        EMPTY
      ))
    )
  );

export const dictionaryEffects:Epic<Action, Action, IAppState, any> = combineEpics(
  loadDictionaryEffect,
  editDictionaryEffect,
  deleteDictionaryEffect,
  onAddDictionaryItemClickEffect,
  newDictionaryClickEffect
);
