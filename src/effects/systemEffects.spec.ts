import { IAppState } from '../models';
import { Actions } from '../actions';
import { ActionTypes } from '../actions/actionTypes';
import { testEpicWithStore } from '../__test_utils__';
import { loadInitialDataEffect } from './systemEffects';

describe('system effects', () => {
  describe('loadInitialDataEffect', () => {
    const state:IAppState = {} as any;

    it('handles fetching of data after application load', async () => {
      // Arrange

      const fetchDictionaries:ActionTypes = {
        type: Actions.FETCH_DICTIONARIES
      };


      // Act
      await testEpicWithStore(loadInitialDataEffect)
        .withState(state)
        .whenReceivesAction({type: Actions.APPLICATION_LOAD})
        .emits(
          fetchDictionaries
        );
    });
  });

});
