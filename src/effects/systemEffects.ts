import { Action, AnyAction } from 'redux';
import { of, concat } from 'rxjs';
import { combineEpics, Epic, ofType } from 'redux-observable';

import { applicationLoad, fetchDictionaries, Actions } from '../actions';
import { IAppState } from '../models';
import { switchMap } from 'rxjs/operators';

/**
 *  @@INIT effect. Load dictionaries when app is initialised.
 */
export const applicationLoadEffect:Epic<AnyAction, AnyAction, IAppState> = () => of(applicationLoad());

export const loadInitialDataEffect:Epic<AnyAction, AnyAction, IAppState> = (action$) =>
  action$.pipe(
    ofType(Actions.APPLICATION_LOAD),
    switchMap(() => concat(
      of(fetchDictionaries())
      )
    )
  );

export const systemEffects:Epic<Action, Action, IAppState, any> = combineEpics(
  applicationLoadEffect,
  loadInitialDataEffect
);
