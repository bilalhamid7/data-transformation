import { IMappedDictionary } from '../models';

export const mapDictionaries = (dictionaries:IMappedDictionary[]) => dictionaries.map(mapDictionary);

// mapper to convert strings to correct types if required
export const mapDictionary = (dictionary:IMappedDictionary) => ({
  ...dictionary
});
