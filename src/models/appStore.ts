import { IDictionaryForm, IMappedDictionary } from './dictionary';

export interface IAppState {
  dictionaries:IMappedDictionary[];
  dictionaryForm:IDictionaryForm;
}
