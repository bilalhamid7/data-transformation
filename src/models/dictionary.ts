export interface IMappedDictionary {
  dictionary:{ [from:string]:string };
  id?:string;
  name:string;
}

export interface IDictionaryForm extends IMappedDictionary {
  from:string;
  to:string;
  isDirty?:boolean;
}
