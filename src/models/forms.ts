import { IDictionaryForm } from './dictionary';

export interface IForm {
  dictionaryForm:IDictionaryForm;
}
