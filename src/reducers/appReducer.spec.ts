import { appReducer as reducer } from './appReducer';
import { defaultDictionaryForm, INITIAL_STATE } from './initialState';
import { IAppState } from '../models';
import { ActionTypes } from '../actions/actionTypes';
import { Actions } from '../actions';
import { dictionaryFactory } from '../__test_utils__/dictionaryFactory';

describe('app reducer', () => {
  it('returns default state when no state is given', () => {
    // Act
    const newState = reducer(undefined, {
      type: 'no-match'
    });

    // Assert
    expect(newState).toEqual(INITIAL_STATE);
  });

  it('returns the same state when action is not matched', () => {
    // Arrange
    const state:IAppState = {
      ...INITIAL_STATE
    };

    // Act
    const newState = reducer(state, {
      type: 'no-match'
    });

    // Assert
    expect(newState).toBe(state);
  });

  describe('dictionaries reducer', () => {
    it('is initialised with an empty array', () => {
      // Act
      const state:IAppState = {
        ...INITIAL_STATE
      };

      // Assert
      expect(state.dictionaries).toEqual([]);
    });

    it('updates dictionaries state when getDictionaries returns success', () => {
      const state:IAppState = {
        ...INITIAL_STATE
      };

      const newDictionariesList = dictionaryFactory.buildMany(1);

      const action:ActionTypes = {
        type: Actions.FETCH_DICTIONARIES_SUCCESS,
        payload: {
          dictionaries: newDictionariesList
        }
      };

      // Act
      const newState = reducer(state, action);

      // Assert
      expect(newState.dictionaries).toEqual(newDictionariesList);
    });
  });

  it('returns the same state when action is not matched', () => {
    // Arrange
    const state:IAppState = {
      ...INITIAL_STATE
    };

    // Act
    const newState = reducer(state, {
      type: 'no-match'
    });

    // Assert
    expect(newState).toBe(state);
  });

  describe('dictionaryForm', () => {
    it('is initialised with empty values by default', () => {
      // Act
      const state:IAppState = {
        ...INITIAL_STATE
      };

      // Assert
      expect(state.dictionaryForm).toBe(defaultDictionaryForm);
    });

    it('updates fields value when they change', () => {
      const state:IAppState = {
        ...INITIAL_STATE
      };

      const action:ActionTypes = {
        type: Actions.UPDATE_FORM_FIELD,
        payload: {
          ticker: 'new value'
        }
      };

      // Act
      const newState = reducer(state, action);

      // Assert
      expect(newState.dictionaryForm).toEqual({
        ...INITIAL_STATE.dictionaryForm,
        ticker: action.payload.ticker
      });
    });
  });

});
