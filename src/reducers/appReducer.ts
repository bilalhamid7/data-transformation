import createReducer from 'redux-action-reducer';
import { Actions } from '../actions';
import { IDictionaryForm, IMappedDictionary } from '../models';
import { AnyAction, combineReducers } from 'redux';
import { IAppState } from '../models';
import { INITIAL_STATE } from './initialState';

/**
 * Using createReducer reduces the amount of boiler plate code written.
 * Multiple different actions can be handled in one reducer.
 *
 * https://www.npmjs.com/package/redux-action-reducer
 */

const dictionaries = createReducer(
  [Actions.FETCH_DICTIONARIES_SUCCESS,
    (state:IMappedDictionary[], payload:{ dictionaries:IMappedDictionary[] }) => payload.dictionaries
  ],
  [Actions.ADD_DICTIONARY, (state:IMappedDictionary[], payload:{ dictionaryForm:IDictionaryForm }) => {
    const index = state.findIndex((item:IMappedDictionary) => item.id === payload.dictionaryForm.id);

    // ID would come back from a real server
    if (index === -1) {
      return [...state, {id: `${state.length + 1}`, dictionary: payload.dictionaryForm.dictionary}]
    }

    return state.map((item:IMappedDictionary, i:number) => {
      if (index === i) {
        return {...item, dictionary: payload.dictionaryForm.dictionary};
      }
      return item;
    })
  }]
)([]);


const dictionaryForm = createReducer(
  [
    Actions.UPDATE_FORM_FIELD,
    (state:IDictionaryForm, payload:{ [id:string]:any }) => {
      return {
        ...state,
        ...payload
      }
    }
  ],
  [Actions.DELETE_DICTIONARY_ITEM, (state:IDictionaryForm, payload:{ key:string }) => {
    const newDictionary = {...state};
    delete newDictionary.dictionary[payload.key];
    return newDictionary;
  }],
  [Actions.EDIT_DICTIONARY_ITEM, (state:IDictionaryForm, payload:{ key:string }) => {
    const key = payload.key;
    const newDictionary = {
      ...state,
      from: key,
      to: state.dictionary[key]
    };
    delete newDictionary.dictionary[payload.key];
    return newDictionary;
  }],
  [Actions.ADD_DICTIONARY_ITEM, (state:IDictionaryForm) => {
    const newDictionary = {
      ...state,
      dictionary: {...state.dictionary, [state.from]: state.to},
      from: '',
      to: '',
      isDirty: false
    };
    return newDictionary;
  }],
)(INITIAL_STATE.dictionaryForm);

export const appReducer = combineReducers<IAppState, AnyAction>({
  dictionaries,
  dictionaryForm
});
