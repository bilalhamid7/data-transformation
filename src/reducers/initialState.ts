import { IAppState, IDictionaryForm } from '../models';

export const defaultDictionaryForm:IDictionaryForm = {
  dictionary: {},
  name: '',
  to: '',
  from: '',
  isDirty: false
};

export const INITIAL_STATE:IAppState = {
  dictionaries: [],
  dictionaryForm: defaultDictionaryForm
};
