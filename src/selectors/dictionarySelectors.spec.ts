import * as selectors from './dictionarySelectors';
import { IAppState } from '../models';
import { dictionaryFactory } from '../__test_utils__/dictionaryFactory';

describe('dictionarySelectors', () => {
  let state:IAppState;

  describe('getDictionaries', () => {
    it('returns empty list when there are no dictionaries', () => {
      // Arrange
      state = {
        dictionaries: []
      } as any;

      // Act
      const result = selectors.getDictionaries(state);

      // Assert
      expect(result).toEqual([]);
    });

    it('returns dictionaries when there are dictionaries created', () => {
      // Arrange
      const dictionaries = dictionaryFactory.buildMany(1);
      state = {
        dictionaries
      } as any;

      // Act
      const result = selectors.getDictionaries(state);

      // Assert
      expect(result).toEqual(dictionaries);
    });
  });
});
