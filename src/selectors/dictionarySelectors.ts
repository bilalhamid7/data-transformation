import { createSelector } from 'reselect';

import { IAppState } from '../models';

export const getDictionaries = createSelector(
  (state:IAppState) => state,
  (state) => state.dictionaries
);

export const getDictionaryForm = createSelector(
  (state:IAppState) => state,
  (state) => state.dictionaryForm
);
