import { formatDate, parseNumberFromString } from './formatters';

describe('formatters', () => {

  describe('formatDate', () => {
    it('returns empty string when date is null or invalid', () => {
      // Arrange

      // Act
      const result = formatDate(null);

      // Assert
      expect(result).toBe('');
    });

    it('returns empty string when date is null or invalid', () => {
      // Arrange

      // Act
      const result = formatDate(new Date('2018-11-07'));

      // Assert
      expect(result).toBe('07-11-2018 00:00');
    });
  });

  describe('parseNumberFromString', () => {
    it('returns null when parameter is not a valid number', () => {
      // Arrange

      // Act
      const result = parseNumberFromString('abc');

      // Assert
      expect(result).toBe(null);
    });

    it('returns a value of type number when a string is passed', () => {
      // Arrange

      // Act
      const result = parseNumberFromString('123.45');

      // Assert
      expect(result).toBe(123.45);
    });
  });

});
