import moment from 'moment';

export const formatDate = (date:Date):string => {
  const momentDate = moment(date);
  return momentDate.isValid() ? momentDate.format('DD-MM-YYYY HH:mm') : '';
};

export const parseNumberFromString = (value:string) => {
  const parsedValue = parseFloat(value);
  return !isNaN(parsedValue) ? parsedValue : null;
};
